### dataset

#### 流程, 获取模块列表

|     | 步骤           | 功能                                                      | 代码                          |
| --- | -------------- | --------------------------------------------------------- | ----------------------------- |
| 1   | 登录           | 登录获取授权                                              | /odoorpc/controllers/web.js   |
|     |                | api.web.session.authenticate({ db, login, password })     | Session.authenticate()        |
|     |                | 暂存 session_info                                         |
| 2   | 模型名         | model='ir.module.module'                                  |
| 3   | 方法名         | method='search_read'                                      |
|     | 模型方法的参数 | domian: []                                                |
|     |                | fields: ['name','display_name']                           |
|     |                | limit:8                                                   |
|     |                | offset:0                                                  |
|     |                | order: 'name'                                             |
| 4   | 调用接口       | api.web.dataset.call_kw(model, mothod, args, kwargs)      | Dataset.call_kw()             |
|     | 组织参数       | args = []                                                 |
|     |                | 从 session_info 中获得 context                            | Session.context               |
|     |                | kwargs = {domian, fields, offset, limit, order, context } |
| 5   | 发送请求       | url: '/web/dataset/call_kw'                               | /odoorpc/request.js           |
|     |                |                                                           | JsonRequest.json_call(url,{}) |
|     |                |

#### 目标

1. 接口越来越多, 创建一个文件夹 controllers 管理所有的接口
2. web.dataset.call_kw 接口, 获取模型数据
3. web.session.context 自定义接口, 获取 context
4. context 用于 web.dataset.call_kw 接口的参数

#### 步骤

1. 复制文件夹 api_demo/odoorpc_t3, 命名为 api_demo/odoorpc_t4
2. 创建文件夹 api_demo/odoorpc_t4/controllers
3. 创建 文件 api_demo/odoorpc_t4/controllers/index.js
4. 创建 文件 api_demo/odoorpc_t4/controllers/web.js
5. 将 api_demo/odoorpc_t4/index.js 中有关接口的代码移动到 web.js 中
6. web.js 中 新增 web.dataset 接口. 补充 web.session 接口
7. 创建测试页面 components/Test4Dataset.vue
8. Test4Dataset 中 调用接口, 显示 odoo 中的模块列表
9. 重复点击按钮, 每次点击, 传不同的 offset 参数, 观察返回数据的变化

#### api_demo/odoorpc_t4/index.js 的内容

```
import { JsonRequest } from './request'

import controller from './controllers'
const { web } = controller

export class RPC {
  constructor() {}

  static init({ baseURL, timeout }) {
    JsonRequest.baseURL = baseURL
    JsonRequest.timeout = timeout
  }
}

const rpc = RPC
rpc.web = web

export default rpc

```

#### api_demo/odoorpc_t4/controllers/index.js 的内容

```
import web from './web'

export default {
  ...web
}

```

#### api_demo/odoorpc_t4/controllers/web.js 的内容

```
import { JsonRequest } from '../request'
JsonRequest._session_info = undefined

class Webclient extends JsonRequest {}

class Dataset extends JsonRequest {
  constructor(payload) {
    super(payload)
  }

  static async call_kw(payload) {
    const { model, method, args, kwargs } = payload
    const url = '/web/dataset/call_kw'
    const url2 = `${url}/${model}/${method}`
    return await this.json_call(url2, { model, method, args, kwargs })
  }
}

class Session extends JsonRequest {
  constructor(payload) {
    super(payload)
  }

  static get session_info() {
    return this._session_info
  }

  static get current_company_id() {
    const { user_companies = {} } = this.session_info || {}
    const { current_company } = user_companies
    return current_company
  }

  static get allowed_company_ids() {
    return [this.current_company_id]
  }

  static get user_context() {
    const session_info = this.session_info
    const context = session_info.user_context || {}
    const allowed_company_ids = this.allowed_company_ids
    return { ...context, allowed_company_ids }
  }

  static get context() {
    return this.user_context
  }

  static async authenticate({ db, login, password }) {
    const url = '/web/session/authenticate'
    const payload = { db, login, password }
    const session_info = await this.json_call(url, payload)
    this._session_info = session_info
    return session_info
  }
}


class Home extends JsonRequest {
  constructor(payload) {
    super(payload)
  }
}

const web = Home
web.webclient = Webclient
web.session = Session
web.dataset = Dataset

export default {
  web
}

```

#### components/Test4Dataset.vue 的内容

```
<script>
import api from '@/api_demo/odoorpc_t4'

const baseURL = process.env.VUE_APP_BASE_API
const timeout = 50000
api.init({ baseURL, timeout })

export default {
  name: 'TestDataset',
  props: {},
  data() {
    return {
      session_info: {},
      context: {},
      modules: [],
      offset: 0
    }
  },
  computed: {},

  async created() {},
  methods: {
    async onclick() {
      const result = await api.web.dataset.call_kw({
        model: 'ir.module.module',
        method: 'search_read',
        args: [],
        kwargs: {
          domain: [],
          fields: ['name', 'display_name'],
          limit: 8,
          offset: this.offset,
          context: api.web.session.context
        }
      })
      console.log('result:', result)

      this.offset = this.offset + 1
      this.modules = result
    },

    async onclickLogin() {
      console.log('click btn')
      const kwargs = { db: 't1', login: 'admin', password: '123456' }
      const result = await api.web.session.authenticate(kwargs)
      console.log('result:', result)
      this.session_info = result
      this.context = api.web.session.context
    }
  }
}
</script>

```
