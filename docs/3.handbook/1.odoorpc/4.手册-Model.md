## Model

### call_kw 接口

##### 调用方法

1. 接口: api.web.dataset.call_kw
2. 参数: model, method, args, kwargs.
   其中 args, kwargs 依据 method, 提供合适的参数.
3. 返回: 根据 method, 返回相应的数据

```
import api from '@/odoorpc'
const test_call_kw = async ()=>{
    const model = 'ir.module.module'
    const method = 'search_read'
    const args = []
    const kwargs = {
        domain: [],
        fields: ['name', 'display_name'],
        limit: 8,
        offset: 0,
        context: api.web.session.context
    }

    const result = await api.web.dataset.call_kw({
        model,
        method,
        args,
        kwargs
    })

    console.log( result )
}

```

##### call_kw 接口的几个使用特点

1. 参数 model 为 模型名
2. 参数 method 为 方法名
3. 参数 args, kwargs 为 method 的参数
4. kwargs 中有个一个参数 context
5. 返回结果的格式, 依赖 method

##### call_kw 接口的进一步封装

1. call_kw 接口, 是一个底层接口. 直接使用, 抽象, 不方便.
2. 对 call_kw 接口, 进一步封装. 以直观的形式使用

```
// 获取 Model:
const Model = api.env.model(model_name, context={})
// 调用模型方法:
await Model.method_name(...args, kwargs)
```

3. api.env.model 缺省使用 api.web.session.context.
4. 在接口调用中, 无需再显式的提供 context 参数

##### 调用模型方法示例

```
import api from '@/odoorpc'
const test_model_method = async ()=>{
    const Model = api.env.model('ir.module.module')
    const domain = []
    const fields = ['name', 'display_name']
    const limit = 8
    const offset = 0
    const order = 'display_name'
    const result = await Model.search_read({
      domain,
      fields,
      offset
      limit,
      order
    })
}

```

### 常用的模型方法

##### 概述

1. 这些方法, 在 odoo 中都有对应的
2. 这些方法的参数及返回值格式, 可参考 odoo

##### execute_kw

1. 参数 method, args, kwargs
2. 调用示例:

```
await Model.execute_kw('search_read', [], {domain, fields})
```

##### execute

1. 参数 method, ...args
2. 调用示例:

```
await Model.execute_kw('search', domain)
```

##### search

1. 参数: domain, {limit, offset, order}
2. 返回: ids. Array 类型.
3. 调用示例:

```
const result = await Model.search([], {limit: 0, offset: 0, order: 'name' })
// 返回result: [1,2,3]
```

##### search_read

1. 参数: domain, {fields, limit, offset, order}
2. 返回: records. Array 类型.
3. 调用示例:

```
const result = await Model.search_read([], {fields:['name'] })
// 返回result: [{id:1, name:'name1'}, {id:2, name:'name1'}]
```

##### web_search_read

1. 参数: domain, {fields, limit, offset, order}
2. 返回: {length, records}.
3. 调用示例:

```
const result = await Model.web_search_read([], {fields:['name']})

// 返回result:
{
    length:10,
    records: [{id:1, name:'name1'}, {id:2, name:'name1'}]
}
```

##### read

1. 参数: id/ids, {fields}
2. 返回: records. Array 类型.
3. 调用示例:

```
const result = await Model.read([1,2], {fields:['name'] })
// 返回result: [{id:1, name:'name1'}, {id:2, name:'name1'}]
```

##### create

1. 参数: vals
2. 返回: id. Integer 类型.
3. 调用示例:

```
const result = await Model.create({name: 'name_value' })
// 返回result: 1
```

##### write

1. 参数: id, vals
2. 返回: true/false
3. 调用示例:

```
const result = await Model.write(1, {name: 'name_value' })
// 返回result: true
```

##### unlink

1. 参数: id/ids
2. 返回: true/false
3. 调用示例:

```
const result = await Model.unlink(1)
// 返回result: true
```

##### name_search

1. 参数: name, args, operator, limit
2. 返回: [ [id1, name ], [id2, name] ]
3. 调用示例:

```
const result = await Model.name_search('', [])
// 返回result: [ [1, 'name' ], [2, 'name2'] ]
```

##### name_get

1. 参数: ids
2. 返回: [ [id1, name ], [id2, name] ]
3. 调用示例:

```
const result = await Model.name_get([1,2])
// 返回 result: [ [1, 'name' ], [2, 'name2'] ]
```

##### onchange

1. 参数: [id], values, field_name, field_onchange
2. 返回: {value: {field1, field2}}
3. 调用示例:

```
const result = await Model.onchange(
    [1],
    {name: 'name1', display_name: 'display_name_old_value'},
    'name',
    {display_name: '1'}
  )

// 返回 result:
{
  value: {display_name: 'display_name_new_value'}
}
```

## many2one

## selection

## many2many

## one2many

## file
