### install module. 演示 Model 继承

#### 创建 odoorpc/addons/ir.module.js 文件

```
import { Model } from '../model'

export class IrModuleModule extends Model {
  constructor(...args) {
    super(...args)
  }

  static async button_immediate_install(module_name) {
    const xml_ref = `base.module_${module_name}`
    const module = await this.env.ref(xml_ref)
    const module_id = module.id
    return await this.execute('button_immediate_install', module_id)
  }

  static async button_immediate_uninstall(module_name) {
    const xml_ref = `base.module_${module_name}`
    const module = await this.env.ref(xml_ref)
    const module_id = module.id
    return await this.execute('button_immediate_uninstall', module_id)
  }

}

const AddonsModels = {
  'ir.module.module': IrModuleModule
}

export default AddonsModels


```

#### 修改文件 odoorpc/env.js 文件

```
import { BaseModel } from './model'

const AddonsFiles = require.context('./addons', true, /\.js$/)

const AddonsModels = AddonsFiles.keys().reduce((models, modulePath) => {
  const value = AddonsFiles(modulePath)
  models = { ...models, ...value.default }
  return models
}, {})

const AllModels = { ...AddonsModels }

export class Environment {

  // ...

  async ref(xml_id) {
    const Model = this.model('ir.model.data')
    const args = ['xmlid_to_res_model_res_id', xml_id, true]
    const [model, res_id] = await Model.execute(...args)
    return { model, id: res_id }
  }

  // ...

  _create_model_class({ model }) {
    const BaseModel2 = AllModels[model] || BaseModel
    class Model extends BaseModel2 {
      // ...
    }

    // ...
  }
}

```

#### 创建测试文件 odoorpc/test/addons/ir_module.js

```
import BaseTestCase from './base'
import rpc from '@/odoorpc'

export default class ModelTestCase extends BaseTestCase {
  async test() {
    await this.list_installed()
    // await this.install()
    // await this.uninstall()
  }

  async list_installed() {
    await this.login()
    const modules_list = await rpc.web.session.modules()
    console.log(modules_list)
  }

  async install() {
    await this.login()
    const model = 'ir.module.module'
    const Model = rpc.env.model(model)
    const modules_list = await rpc.web.session.modules()

    const module_name = 'contacts'
    if (modules_list.includes(module_name)) {
      console.log(module_name, ' is installed.  can not install again')
      return
    }

    const res = await Model.button_immediate_install(module_name)
    console.log(res)
  }

  async uninstall() {
    await this.login()

    const model = 'ir.module.module'
    const Model = rpc.env.model(model)
    const modules_list = await rpc.web.session.modules()

    const module_name = 'contacts'
    if (!modules_list.includes(module_name)) {
      console.log(module_name, ' is not installed.  can not uninstall')
      return
    }

    const res = await Model.button_immediate_uninstall(module_name)
    console.log(res)
  }
}

```

#### 测试

1. 调用 rpc.web.database.create 函数, 创建一个数据库 test_db
2. 修改 test_rpc.js 里的 login_info.db = test_db
3. 调用 模块安装函数

### 使用指南

1. 模块名为参数, 调用 模块的安装函数, 安装模块
2. 服务端模块安装时间比较长, 前端应 阻止页面的操作额外操作, 防止重复点击
