const ModelFields = {
  display_name: { disable_field_onchange: 1 }
}

const AddonsFields = {
  'account.group': ModelFields
}

export default AddonsFields
