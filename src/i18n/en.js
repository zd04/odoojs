export default {
  mainTitle: {
    projectTitle: 'Welcome odoojs',
    supplier: 'Beijing BMX Tech. Co., Ltd',
    copyright: '©2023'
  },

  act: {
    confirm: 'ok',
    cancel: 'cancel',
    delete: 'delete',
    edit: 'edit',
    save: 'save',
    back: 'back',
    create: 'create',
    deleteTip: 'Are you sure to delete this data?',
    inputNameTip: 'Please input user name.',
    exportAll: 'Export All'
  },
  resUser: {
    account: 'Account',
    name: 'User Name',
    phone: 'Phone',
    currentComp: 'Current Company',
    companies: 'Companies',
    latestLogin: 'Latest Login'
  },
  timeSearch: {
    today: 'today',
    oneMonth: 'one month',
    threeMonths: 'three months',
    sixMonths: 'six months'
  },
  showMoreSearch: {
    advancedSearch: 'advanced search',
    close: 'close'
  }
}
