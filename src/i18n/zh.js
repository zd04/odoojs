export default {
  mainTitle: {
    projectTitle: '欢迎使用 odoojs',
    supplier: '北京斑马线科技有限公司',
    copyright: '©2021'
  },

  act: {
    confirm: '确认',
    cancel: '取消',
    delete: '删除',
    edit: '编辑',
    save: '保存',
    back: '返回',
    create: '创建',
    deleteTip: '您是要删除这条数据吗?',
    inputNameTip: '请输入用户名',
    exportAll: '导出全部'
  },
  resUser: {
    account: '账号',
    name: '用户名',
    phone: '电话',
    currentComp: '当前公司',
    companies: '公司',
    latestLogin: '最近登录'
  },
  timeSearch: {
    today: '今天',
    oneMonth: '近一个月',
    threeMonths: '近三个月',
    sixMonths: '近半年'
  },
  showMoreSearch: {
    advancedSearch: '高级搜索',
    close: '关闭'
  }
}
